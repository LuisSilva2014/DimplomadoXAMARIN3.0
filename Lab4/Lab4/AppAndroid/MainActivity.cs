﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace AppAndroid
{
    [Activity(Label = "AppAndroid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            /*Creamos la instancia del codigo compartido y le inyectamos la dependencia*/
            var Validator = new PCLProyect.AppValidator(new AndroidiDialog(this));

            string myDevice = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            //XamarinDiplomado.ServiceHelper helper = new XamarinDiplomado.ServiceHelper();

            string Student = "l.r.s.m.2009@hotmail.com";
            string pws = "luissilva2013";

            Validator.Device = myDevice;
            Validator.Email = Student;
            Validator.Pwd = pws;
            //Realizamos la validacion
            Validator.Validate();

        }
    }
}

