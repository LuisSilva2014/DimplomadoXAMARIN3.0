﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace Lab9
{
    [Activity(Label = "@string/ApplicationName", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
             SetContentView (Resource.Layout.Main);
            Validate("l.r.s.m.2009@hotmail.com", "luissilva2013");

        }

        async void Validate(string Student, string pws)
        {
            SALLab09.ServiceClient ServiceClient = new SALLab09.ServiceClient();
            string myDevice = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            SALLab09.ResultInfo R = await ServiceClient.ValidateAsync(Student, pws, myDevice);

            SetContentView(Resource.Layout.Main);
            var UserNameTextValue = FindViewById<TextView>(Resource.Id.UserNameTextValue);
            var StatusValue = FindViewById<TextView>(Resource.Id.StatusValue);
            var TokenTextValue = FindViewById<TextView>(Resource.Id.TokenTextValue);
            UserNameTextValue.Text = $"{R.Status}";
            StatusValue.Text = $"{R.Fullname}";
            TokenTextValue.Text = $"{R.Token}";
        }
    }
}

