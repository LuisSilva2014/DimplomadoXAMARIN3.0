﻿using Android.App;
using Android.Widget;
using Android.OS;



namespace AndroidApp
{
    [Activity(Label = "AndroidApp", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private async void Validate()
        {
            
            SALLab02.ServiceClient ServiceClient = new SALLab02.ServiceClient();
            string myDevice = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            //XamarinDiplomado.ServiceHelper helper = new XamarinDiplomado.ServiceHelper();

            string Student = "l.r.s.m.2009@hotmail.com";
            string pws = "luissilva2013";
            SALLab02.ResultInfo R = await ServiceClient.ValidateAsync(Student, pws, myDevice);

            Android.App.AlertDialog.Builder Builder = new AlertDialog.Builder(this);

            AlertDialog Alerta = Builder.Create();
            Alerta.SetTitle("Resultado de la verificación");
            Alerta.SetIcon(Resource.Drawable.Icon);
            Alerta.SetMessage($"{R.Status}\n{R.Fullname}\n{R.Token}");

            Alerta.SetButton("Ok", (s, ev) => { });

            Alerta.Show();

        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Validate();
        }

    }
}

