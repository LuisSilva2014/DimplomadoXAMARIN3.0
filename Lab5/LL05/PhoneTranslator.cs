﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace LL05
{
    public class PhoneTranslator
    {
        string Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string Numbers = "22233344455566677778889999";

        public string ToNumber(string alphanumericonumber)
        {

            var NumericPhoneNumeber = new StringBuilder();

            if (!string.IsNullOrEmpty(alphanumericonumber))
            {
                alphanumericonumber = alphanumericonumber.ToUpper();
                foreach (var cin in alphanumericonumber)
                {
                    if ("0123456789".Contains(cin))
                    {
                        NumericPhoneNumeber.Append(cin);
                    }
                    else
                    {
                        var Index = Letters.IndexOf(cin);
                        if (Index >= 0)
                        {
                            NumericPhoneNumeber.Append(Numbers[Index]);
                        }
                    }
                }
            }
            return NumericPhoneNumeber.ToString();
        }
    }
}