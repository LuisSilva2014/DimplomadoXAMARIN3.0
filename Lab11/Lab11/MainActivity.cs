﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;

namespace Lab11
{
    [Activity(Label = "Lab11", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        Complex Data;
        int Counter = 0;
        ComplexValidate DataValidate;
        string ResultTokenCredentials = "";

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            //Al ejecutarse se enviara un LOG al crear el evento OnCreate
            Android.Util.Log.Debug("Lab11Log", "Activity A - OnCreate");
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            //Este codigo persmete que el manegar al presionar clic sobre el boton StartActivity  inicie la segunda activitdad
            FindViewById<Button>(Resource.Id.StartActivity).Click += (s, e) =>
                {

                    var ActiviyIntent = new Android.Content.Intent(this, typeof(SecondActivity));
                    StartActivity(ActiviyIntent);
                };


            //Utilzia el FragmentManager para recuperar el fragmento
            Data = (Complex)this.FragmentManager.FindFragmentByTag("Data");
            if (Data == null)
            {
                //No ha sido almacenado, se agrega el fragmento a la actividad
                Data = new Complex();
                var FragmentTransition = this.FragmentManager.BeginTransaction();
                FragmentTransition.Add(Data, "Data");
                FragmentTransition.Commit();
            }

            //Persistencia de información al antenticarse
            DataValidate = (ComplexValidate)this.FragmentManager.FindFragmentByTag("DataValidate");
            if (DataValidate == null)
            {
                ///Valdiacion de la actividad en azure mobile
                Validate("l.r.s.m.2009@hotmail.com", "luissilva2013");
                //No ha sido almacenado, se agrega el fragmento a la actividad
                DataValidate = new ComplexValidate();
                var FragmentTransition = this.FragmentManager.BeginTransaction();
                FragmentTransition.Add(DataValidate, "DataValidate");
                FragmentTransition.Commit();
            }

            if (bundle != null)
            {
                Counter = bundle.GetInt("CounterValue", 0);
                ResultTokenCredentials = bundle.GetString("ResultTokenCredentialsValue", "");
                Android.Util.Log.Debug("Lab11Log", "Activity A - Recovered Instance State");
            }
            var ClickCounter = FindViewById<Button>(Resource.Id.ClickCounter);
            var TextResul = FindViewById<TextView>(Resource.Id.textView3);
            ClickCounter.Text += $"\n{Data.ToString()}";
            TextResul.Text += $"\n{DataValidate.Result}";

            ClickCounter.Click += (s, e) =>
            {
                Counter++;
                ClickCounter.Text = Resources.GetString(Resource.String.ClickCounter_Text, Counter);
                TextResul.Text += ResultTokenCredentials;

                //Modificar con cualqueir valor solo para verificar la persisitencia
                Data.Real++;
                Data.Imaginary++;
                //Mostrar el valor de los miembros
                ClickCounter.Text += $"\n{Data.ToString()}";
            };
        }
        async void Validate(string Student, string pws)
        {
            SALLab11.ServiceClient ServiceClient = new SALLab11.ServiceClient();
            string myDevice = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            SALLab11.ResultInfo R = await ServiceClient.ValidateAsync(Student, pws, myDevice);
            SetContentView(Resource.Layout.Main);
            var TextResul = FindViewById<TextView>(Resource.Id.textView3);
            DataValidate.Result = $"{R.Status}\n{R.Fullname}\n{R.Token}";
            TextResul.Visibility = ViewStates.Visible;
            //Por carga asincrona, se procede a Carga de datos al textResul la vez 
            TextResul.Text += $"\n{DataValidate.Result}";
        }


        //Este metodo siempre se ejecuta despues de OnCreate
        protected override void OnStart()
        {
            Android.Util.Log.Debug("Lab11Log", "Activity A - OnStart");
            base.OnStart();
        }


        /// <summary>
        /// OnResume es el metodo ejecutado despues de OnStart
        /// 
        /// Este metodo nos permite sobreescribir acciones como: 
        /// * Aumento de velociddad de frame (tarea comun entre juegos)
        /// * Iniciar animaciones
        /// * Escucahr actulaizacionde GPS
        /// * Mostrar cualquier alert o notificacion relevante
        /// * Conectar con menejadores d e evnetos externos
        /// Este metodo es el unico que nos da la garantia de ejecutarse despues del metodo OnPause cuando traemos la actividad de regreso
        /// </summary>
        protected override void OnResume()
        {
            Android.Util.Log.Debug("Lab11Log", "Activity A - OnResume");
            base.OnResume();
        }


        /// <summary>
        /// OnPause es el metodo ejecutado cuando el sistema esta apunto de poner la activity en segundo plano
        /// 
        /// Este metodo nos permite sobreescribir acciones como: 
        /// * Guardar los cambios pendiente para la persistencia de datos
        /// * Destruir o libera obetos pendiente..
        /// </summary>
        protected override void OnPause()
        {
            Android.Util.Log.Debug("Lab11Log", "Activity A - OnPause");
            base.OnPause();
        }


        //OnResume y OnStop son los metodos que se pueden invocar despues de OnPause()
        //Este metodo se ejeuta cunado la activity no esta visible para el usuario
        protected override void OnStop()
        {
            Android.Util.Log.Debug("Lab11Log", "Activity A - OnStop");
            base.OnStop();
        }

        //Este metodo se ejeuta antes de que la actividad sea elimindad de memoria
        protected override void OnDestroy()
        {
            Android.Util.Log.Debug("Lab11Log", "Activity A - OnDestroy");
            base.OnDestroy();
        }

        //Este metodo se ejeuta antes de que la actividad se intente reiniciar al primier plano
        protected override void OnRestart()
        {
            Android.Util.Log.Debug("Lab11Log", "Activity A - OnRestart");
            base.OnRestart();
        }


        //bundle DEBE SER USADO PARA VALORES SIMPLES: ENTEROS O STRING, NO PARA VALORES COMPLEJOS
        //Este metodo nos permite guardar el estado de la instacia al tratar de reiniicarse y asi guardar los datos
        protected override void OnSaveInstanceState(Bundle outState)
        {
            //OnSaveInstanceState funciona muy bien solo para guiardar los datos transitorios de la actividad. 
            //El en el caso del EditText que exista se ra persisitined¿so la info aun si esta gira, ya que tiene un ID en el layou pero si se quita se perdera.


            //Limitaciones: no se invoca lapresionar HOME o BACK, no esta diseñado para objetos grandes como imagenes, los datos son serialiozados y pueden provocar retrasos
            outState.PutInt("CounterValue", Counter);
            outState.PutString("ResultTokenCredentialsValue", ResultTokenCredentials);

            Android.Util.Log.Debug("Lab11Log", "Activity A - OnSaveInstanceState");
            base.OnSaveInstanceState(outState);
        }

    }
}


