﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace LL05
{
    [Activity(Label = "@string/ValidateString", MainLauncher = false, Icon = "@drawable/icon")] 
    public class ValidateActivity : Activity 
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
            
            //Configuramos esta activada para que se vea en el diseño: Validate.asmx
            SetContentView(Resource.Layout.Validate);
            // Create your application here
            var btnValidate = FindViewById<Button>(Resource.Id.btnValidate);
            var TextResul = FindViewById<TextView>(Resource.Id.textView3);
            TextResul.Visibility = ViewStates.Invisible;

            btnValidate.Click += (object sender, System.EventArgs e) =>
            {
                var TxtEmail = FindViewById<EditText>(Resource.Id.TxtEmail);
                var TxtPassword = FindViewById<EditText>(Resource.Id.TxtPassword);
                TextResul.Visibility = ViewStates.Visible;
                Validate(TxtEmail.Text, TxtPassword.Text);
            };
        }

        async void Validate(string Student, string pws)
        {
            SALLab06.ServiceClient ServiceClient = new SALLab06.ServiceClient();
            string myDevice = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            SALLab06.ResultInfo R = await ServiceClient.ValidateAsync(Student, pws, myDevice);

            SetContentView(Resource.Layout.Validate);
            var TextResul = FindViewById<TextView>(Resource.Id.textView3);
            TextResul.Text = $"{R.Status}\n{R.Fullname}\n{R.Token}";
        }

    }
}