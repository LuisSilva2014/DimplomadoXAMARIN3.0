﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SharedProject
{
    class ClassShared
    {

        /// <summary>
        /// un proyecto compartido no es compilado cuando n existan algo que lo referencie, 
        /// por eso los errores de sintaxis (o cualquier otro error) no seran señalados 
        /// hasta que haya sido referenciado por alguien mas
        /// </summary>
        /// <param name="FileName"></param>
        /// 
        /// El contexto del proyeto no tiene opciones de compilacion
        /// <returns></returns>
        public string GetFilePath ( string FileName)
        {
#if WINDOWS_UWP
            var FilePath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, FileName);

#else
#if __ANDROID__
            string LibraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); ;
            var FilePath = Path.Combine(LibraryPath, FileName);
#endif
#endif
            return FilePath;
        }

    }
}
