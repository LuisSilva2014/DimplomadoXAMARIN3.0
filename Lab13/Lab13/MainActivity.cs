﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace Lab13
{
    [Activity(Label = "Lab13", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            //bOTON ASINCRONO
            FindViewById<Button>(Resource.Id.button1).Click += async (sS, e) =>
            {
                SALLab13.ServiceClient ServiceClient = new SALLab13.ServiceClient();
                string Student = "l.r.s.m.2009@hotmail.com";
                string pws = "luissilva2013";
                SALLab13.ResultInfo R = await ServiceClient.ValidateAsync(this, Student, pws);

                Android.App.AlertDialog.Builder Builder = new AlertDialog.Builder(this);
                AlertDialog Alerta = Builder.Create();
                Alerta.SetTitle("Resultado de la verificación lab 13");
                Alerta.SetIcon(Resource.Drawable.Icon);
                Alerta.SetMessage($"{R.Status}\n{R.FullName}\n{R.Token}");
                Alerta.SetButton("Ok", (s, ev) => { });
                Alerta.Show();
                //Validate();
            };
            
        }

        async void Validate()
        { 
            SALLab13.ServiceClient ServiceClient = new SALLab13.ServiceClient();
            string Student = "l.r.s.m.2009@hotmail.com";
            string pws = "luissilva2013";
            SALLab13.ResultInfo R = await ServiceClient.ValidateAsync(this, Student, pws);

            Android.App.AlertDialog.Builder Builder = new AlertDialog.Builder(this);
            AlertDialog Alerta = Builder.Create();
            Alerta.SetTitle("Resultado de la verificación lab 13");
            Alerta.SetIcon(Resource.Drawable.Icon);
            Alerta.SetMessage($"{R.Status}\n{R.FullName}\n{R.Token}");
            Alerta.SetButton("Ok", (s, ev) => { });
            Alerta.Show();
        }
    }
}

