﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace LL05
{
    [Activity(Label = "Lab 05 | Diplomado Xamarin", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            var pPhoneNumberText = FindViewById<EditText>(Resource.Id.PhoneNumberText);
            var translateButton = FindViewById<Button>(Resource.Id.TranslateButton);
            var callButton = FindViewById<Button>(Resource.Id.CallButton);


            callButton.Enabled = false;
            var trasnlatedNumber = string.Empty;

            translateButton.Click += (object sender, System.EventArgs e) =>
            {
                var translator = new PhoneTranslator();
                trasnlatedNumber = translator.ToNumber(pPhoneNumberText.Text);
                if (string.IsNullOrEmpty(trasnlatedNumber))
                {
                    //No hay numero a llamar
                    callButton.Text = "Llamar";
                    callButton.Enabled = false;
                }
                else
                {
                    //Hya un posoible numero a llamar
                    callButton.Text = $"Llamar al { trasnlatedNumber}";
                    callButton.Enabled = true;
                }
            };


            callButton.Click += (object sender, System.EventArgs e) =>
            {
                //intentar marcar al nuemro de telefono
                var CallDialog = new AlertDialog.Builder(this);
                CallDialog.SetMessage($"Llamar al numero {trasnlatedNumber}? ");

                CallDialog.SetNeutralButton("Llamar", delegate
                {


                    //Crear un intento de llamar al numero telefonico
                    var CallIntent =
                    new Android.Content.Intent(Android.Content.Intent.ActionCall);

                    //CallIntent.SetData(Android.Net.Uri.Parse($"tel: {trasnlatedNumber}? "));
                    CallIntent.SetData(Android.Net.Uri.Parse($"tel:{trasnlatedNumber}"));
                    StartActivity(CallIntent);
                });

                CallDialog.SetNegativeButton("Cancelar", delegate { });
                //Muestra el cuadro de dialogo al usuario y espera una respuesta


                CallDialog.Show();


                //Los permisos se configura en el propertis> mainifesto y permitir CALLPHONE
            };

            //Validate();


            //async Task<string> Validate()
        
        }

        async void Validate()
        {

            SALLab05.ServiceClient ServiceClient = new SALLab05.ServiceClient();
            string myDevice = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            //XamarinDiplomado.ServiceHelper helper = new XamarinDiplomado.ServiceHelper();

            string Student = "l.r.s.m.2009@hotmail.com";
            string pws = "luissilva2013";
            SALLab05.ResultInfo R = await ServiceClient.ValidateAsync(Student, pws, myDevice);

            //SetContentView(Resource.Layout.Main);
            var TextResul = FindViewById<TextView>(Resource.Id.textView2);
            TextResul.Text = $"{R.Status}\n{R.Fullname}\n{R.Token}";
            //TextResul.Text = $"Si jajajaj";

        }
    }
}

