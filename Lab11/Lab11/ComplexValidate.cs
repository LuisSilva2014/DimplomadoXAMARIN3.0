﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Lab11
{
    public class ComplexValidate : Fragment
    {
        public string Result { get; set; }
        //public int Imaginary { get; set; }

        //public override string ToString()
        //{
        //    return $"{Real} + {Imaginary}i";
        //}

        //Metodo que permite a aindroid retner la  instancia acutal  del fragmento de una activuda. Esto nos permite perservar informacion mas cmpleja en una  perdida de internet pro ejemplo
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RetainInstance = true;  //Asi evitamos en el clcilo de vida de la actividad la destruccion
        }
    }
}