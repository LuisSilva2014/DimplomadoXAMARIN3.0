﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UWP
{
    class UWPDialog : PCLProyect.iDialog
    {

        public async void Show (string Mes)
        {
            var Dialog = new Windows.UI.Popups.MessageDialog(Mes);
            await Dialog.ShowAsync();
        } 
    }
}
