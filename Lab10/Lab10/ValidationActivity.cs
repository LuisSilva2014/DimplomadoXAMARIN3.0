﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Lab10
{
    [Activity(Label = "@string/ApplicationName", MainLauncher = true, Icon = "@drawable/icon")]
    public class ValidationActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ValidationLayout);
            // Create your application here
            var btnValidate = FindViewById<Button>(Resource.Id.btnValidate);
            var TextResul = FindViewById<TextView>(Resource.Id.textView3);
            TextResul.Visibility = ViewStates.Invisible;

            btnValidate.Click += (object sender, System.EventArgs e) =>
            {
                var TxtEmail = FindViewById<EditText>(Resource.Id.TxtEmail);
                var TxtPassword = FindViewById<EditText>(Resource.Id.TxtPassword);

                Validate(TxtEmail.Text, TxtPassword.Text);
            };
        }

        async void Validate(string Student, string pws)
        {
            SALLab10.ServiceClient ServiceClient = new SALLab10.ServiceClient();
            string myDevice = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            SALLab10.ResultInfo R = await ServiceClient.ValidateAsync(Student, pws, myDevice);

            SetContentView(Resource.Layout.ValidationLayout);
            var TextResul = FindViewById<TextView>(Resource.Id.textView3);

            if (Android.OS.Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                //Debido a que las notificaciones fueron incluidas despues de la version 5.0 del nivel 19 api de android.
                //Si no se especifa la version el
                //sigueinte  codigo debera generar error en tiempo de ejecucion en versiones antiguas

                var Builder = new Notification.Builder(this)
                    .SetContentTitle("Resultado de la valdiación")
                    .SetContentText($"{R.Status}\n{R.Fullname}\n{R.Token}")
                    .SetSmallIcon(Resource.Drawable.Icon);
                Builder.SetCategory(Notification.CategoryMessage);

                //Ensvair la notificacion a la pantalla 
                var ObjNotificacion = Builder.Build();
                var Manager = GetSystemService(Android.Content.Context.NotificationService) as NotificationManager;

                Manager.Notify(0, ObjNotificacion);


                TextResul.Visibility = ViewStates.Visible;
                TextResul.Text = $"{R.Status}\n{R.Fullname}\n{R.Token}";
            }
            else
            {
                ///Ejecutamos apra versiones antifuoias a 5.0
                /// Aqui SetCategory no disponible
                TextResul.Visibility = ViewStates.Visible;
                TextResul.Text = $"{R.Status}\n{R.Fullname}\n{R.Token}";
            }
        }
    }
}