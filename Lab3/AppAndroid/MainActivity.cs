﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace AppAndroid
{
    [Activity(Label = "AppAndroid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            // SetContentView (Resource.Layout.Main);

            var helper = new SharedProject.ClassShared();
            new AlertDialog.Builder(this)
                .SetMessage(helper.GetFilePath("demo.dat"))
                .Show();

            Validate();

        }


        private async void Validate()
        {

            SALLab03.ServiceClient ServiceClient = new SALLab03.ServiceClient();
            string myDevice = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            //XamarinDiplomado.ServiceHelper helper = new XamarinDiplomado.ServiceHelper();

            string Student = "l.r.s.m.2009@hotmail.com";
            string pws = "luissilva2013";
            SALLab03.ResultInfo R = await ServiceClient.ValidateAsync(Student, pws, myDevice);

            Android.App.AlertDialog.Builder Builder = new AlertDialog.Builder(this);

            AlertDialog Alerta = Builder.Create();
            Alerta.SetTitle("Resultado de la verificación lab 3");
            Alerta.SetIcon(Resource.Drawable.Icon);
            Alerta.SetMessage($"{R.Status}\n{R.Fullname}\n{R.Token}");

            Alerta.SetButton("Ok", (s, ev) => { });

            Alerta.Show();

        }



    }
}

