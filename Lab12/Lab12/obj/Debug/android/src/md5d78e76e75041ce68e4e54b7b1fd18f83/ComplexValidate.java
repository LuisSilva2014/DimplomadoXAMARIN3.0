package md5d78e76e75041ce68e4e54b7b1fd18f83;


public class ComplexValidate
	extends android.app.Fragment
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("Lab12.ComplexValidate, Lab12, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", ComplexValidate.class, __md_methods);
	}


	public ComplexValidate () throws java.lang.Throwable
	{
		super ();
		if (getClass () == ComplexValidate.class)
			mono.android.TypeManager.Activate ("Lab12.ComplexValidate, Lab12, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
