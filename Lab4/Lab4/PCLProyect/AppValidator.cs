﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCLProyect
{
    public class AppValidator
    {

        iDialog Dialog;
        public AppValidator(iDialog platformDialog)
        {
            Dialog = platformDialog;
        }


        public string Email{ get; set; }
        public string Pwd { get; set; }
        public string Device { get; set; }
        public async void Validate()
        {
            string Result;
            //Result = "Aplicación validada";

            var ServiceCliente = new SALLab04.ServiceClient();
            var SvcResult = await ServiceCliente.ValidateAsync(Email, Pwd, Device);

            Result = $"{SvcResult.Status}\n{SvcResult.Fullname}\n{SvcResult.Token}";
            //Invocar el condigo especifico de la plataforma
            Dialog.Show(Result);
        }

    }
}