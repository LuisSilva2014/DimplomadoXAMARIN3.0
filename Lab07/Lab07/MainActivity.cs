﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;

namespace Lab07
{
    [Activity(Label = "Lab07", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);

            //Configuramos esta activada para que se vea en el diseño: Validate.asmx
            SetContentView(Resource.Layout.Main);
          

            // Create your application here
            var btnValidate = FindViewById<Button>(Resource.Id.btnValidate);
            var TextResul = FindViewById<TextView>(Resource.Id.textView3);
            TextResul.Visibility = ViewStates.Invisible;

            btnValidate.Click += (object sender, System.EventArgs e) =>
            {
                var TxtEmail = FindViewById<EditText>(Resource.Id.TxtEmail);
                var TxtPassword = FindViewById<EditText>(Resource.Id.TxtPassword);
            
                Validate(TxtEmail.Text, TxtPassword.Text);
            };
        }

        async void Validate(string Student, string pws)
        {
            SALLab07.ServiceClient ServiceClient = new SALLab07.ServiceClient();
            string myDevice = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            SALLab07.ResultInfo R = await ServiceClient.ValidateAsync(Student, pws, myDevice);

            SetContentView(Resource.Layout.Main);
            var TextResul = FindViewById<TextView>(Resource.Id.textView3);


            if (Android.OS.Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                //Debido a que las notificaciones fueron incluidas despues de la version 5.0 del nivel 19 api de android.
                //Si no se especifa la version el
                //sigueinte  codigo debera generar error en tiempo de ejecucion en versiones antiguas
                
                var Builder = new Notification.Builder(this)
                    .SetContentTitle("Resultado de la valdiación")  
                    .SetContentText($"{R.Status}\n{R.Fullname}\n{R.Token}")
                    .SetSmallIcon(Resource.Drawable.Icon);
                Builder.SetCategory(Notification.CategoryMessage);

                //Ensvair la notificacion a la pantalla 
                var ObjNotificacion = Builder.Build();
                var Manager = GetSystemService(Android.Content.Context.NotificationService) as NotificationManager;

                Manager.Notify(0, ObjNotificacion);







                TextResul.Visibility = ViewStates.Visible;
                TextResul.Text = $"{R.Status}\n{R.Fullname}\n{R.Token}";
            }
            else
            {
                ///Ejecutamos apra versiones antifuoias a 5.0
                /// Aqui SetCategory no disponible
                 TextResul.Visibility = ViewStates.Visible;
                 TextResul.Text = $"{R.Status}\n{R.Fullname}\n{R.Token}";
            }
        }
    }
}

