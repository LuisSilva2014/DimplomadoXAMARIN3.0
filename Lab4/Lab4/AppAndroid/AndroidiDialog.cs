﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AppAndroid
{
    class AndroidiDialog : PCLProyect.iDialog
    {
        Context AppContexto;
        public AndroidiDialog (Context Contexto)
        {
            AppContexto = Contexto;
        }


        public void Show(string mes)
        {
            Android.App.AlertDialog.Builder b =
                new AlertDialog.Builder(AppContexto);
            AlertDialog Alert = b.Create();
            Alert.SetTitle("Resultado de la verificación");
            Alert.SetIcon(Resource.Drawable.Icon);
            Alert.SetMessage(mes);
            Alert.SetButton("OK", (s, ev) => { });
            Alert.Show();
        }
    }
}