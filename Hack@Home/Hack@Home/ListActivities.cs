﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HackAtHome.SAL;
using HackAtHome.CustomAdapters;
using System.Threading.Tasks;
using HackAtHome.Entities;
    
namespace Hack_Home
{
    [Activity(Label = "ListActivities")]
    public class ListActivities : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ListItem);
            
            string TOKEN = Intent.GetStringExtra("ResultTokenCredentialsValue") ?? "Data not available";
            var textViewTitle = FindViewById<TextView>(Resource.Id.textViewTitle);
            CargarDataEvidencia(TOKEN);
        }

        private async void CargarDataEvidencia(string TK)
        {
            var ListEvidences = FindViewById<ListView>(Resource.Id.listView1);
            ServiceClient Obj = new ServiceClient();
            var LS = await Obj.GetEvidencesAsync(TK);

            ListEvidences.Adapter = new EvidencesAdapter(
            this, LS, Resource.Layout.ListItem, Resource.Id.textViewTitle, Resource.Id.textViewTitle);
        }
    }
}