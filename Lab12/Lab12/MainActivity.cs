﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using System;
using Android.Graphics;

namespace Lab12
{
    [Activity(Label = "Lab12", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {

        ComplexValidate DataValidate;
        string ResultTokenCredentials = "";
        string User = "l.r.s.m.2009@hotmail.com";
        string pass = "luissilva2013";

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            // SetContentView (Resource.Layout.Main);

            //var MyLayout = new MyViewGroup(this);
            //SetContentView(MyLayout);

            Validate();
        }
        async void Validate()
        {
            //CONFIGURACIÓN DE LAS VISTAS
            SetContentView(Resource.Layout.Main);

            var ListColor = FindViewById<ListView>(Resource.Id.listView1);
            ListColor.Adapter = new CustomAdapters.ColorAdapter(
                this, Resource.Layout.ListItem, Resource.Id.textView1, Resource.Id.textView2, Resource.Id.imageView1);



            //VALIDACIÓN DEL LABORATORIO
            SALLab12.ServiceClient ServiceClient = new SALLab12.ServiceClient();
            string myDevice = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            SALLab12.ResultInfo R = await ServiceClient.ValidateAsync(User, pass, myDevice);
            var TextResul = FindViewById<TextView>(Resource.Id.textView3);
            TextResul.Text = $"{R.Status}\n{R.FullName}\n{R.Token}";
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            //OnSaveInstanceState funciona muy bien solo para guiardar los datos transitorios de la actividad. 
            //El en el caso del EditText que exista se ra persisitined¿so la info aun si esta gira, ya que tiene un ID en el layou pero si se quita se perdera.


            //Limitaciones: no se invoca lapresionar HOME o BACK, no esta diseñado para objetos grandes como imagenes, los datos son serialiozados y pueden provocar retrasos
            outState.PutString("ResultTokenCredentialsValue", ResultTokenCredentials);

            Android.Util.Log.Debug("Lab11Log", "Activity A - OnSaveInstanceState");
            base.OnSaveInstanceState(outState);
        }

    }

    class MyViewGroup: ViewGroup
    {
        Context ViewGroupContext;
        public MyViewGroup(Context context) : base(context)
        {
            this.ViewGroupContext = context;
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            // UN View ES un color de pantalla con el que el usuario puede interacturar.
            this.SetBackgroundColor(Color.Fuchsia);
            var MyView = new View(ViewGroupContext);

            MyView.SetBackgroundColor(Color.Blue);
            MyView.Layout(20, 20, 150, 150);
            AddView(MyView);
        }


        

    }
}

