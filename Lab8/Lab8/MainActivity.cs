﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;

namespace Lab8
{
    [Activity(Label = "@string/ApplicationName", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource

            //Se estable el diñseo de interfaz  de usuario para la actividad
            SetContentView(Resource.Layout.Main);


            //Obtener la vista la raiz ppal
            //var ViewGroup = (Android.Views.ViewGroup)Window.DecorView.FindViewById(Android.Resource.Id.Content);
            //var MainLayout = ViewGroup.GetChildAt(0) as LinearLayout;

            ////Hacer referencia a un tipo de ImageView

            //var HeadImage = new ImageView(this);
            ////Se lalma a la imagen agregada 
            //HeadImage.SetImageResource(Resource.Drawable.Xamarin_Diplomado_30);
            ////Se incorpora el elemeton de Imagen a la main
            //MainLayout.AddView(HeadImage);


            //var UserNameTextView = new TextView(this);
            ////Ahora obtenemos el nobre del recurso del string 
            ////UserName

            //UserNameTextView.Text = GetString(Resource.String.UserName);
            ////Se incorpora
            //MainLayout.AddView(UserNameTextView);

            Validate("l.r.s.m.2009@hotmail.com", "luissilva2013");

        }

        async void Validate(string Student, string pws)
        {
            SALLab08.ServiceClient ServiceClient = new SALLab08.ServiceClient();
            string myDevice = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            SALLab08.ResultInfo R = await ServiceClient.ValidateAsync(Student, pws, myDevice);

            SetContentView(Resource.Layout.Main);
            var UserNameTextValue = FindViewById<TextView>(Resource.Id.UserNameTextValue);
            var StatusValue = FindViewById<TextView>(Resource.Id.StatusValue);
            var TokenTextValue = FindViewById<TextView>(Resource.Id.TokenTextValue);
            UserNameTextValue.Text = $"{R.Status}";
            StatusValue.Text = $"{R.Fullname}";
            TokenTextValue.Text = $"{R.Token}";
        }
    }
}

