﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace Lab10
{
    [Activity(Label = "@string/ApplicationName", MainLauncher = false, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int Couter = 0;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);

            var CounterHeader = FindViewById<TextView>(Resource.Id.CounterHeader);
            CounterHeader.Text = GetText(Resource.String.ContenHeader);

            var ClickMe = FindViewById<TextView>(Resource.Id.ClickMe);
            var ClickCounter = FindViewById<TextView>(Resource.Id.ClickCounter);
            ClickMe.Click += (s,e) =>
            {
                Couter++;
                ClickCounter.Text = Resources.GetQuantityString(Resource.Plurals.numbersOfClicks, Couter, Couter);

                //Cargamos la pista para el idioma correspindiete  sound.mp3
                var Player = Android.Media.MediaPlayer.Create(this, Resource.Raw.sound);
                Player.Start(); 
            };

            Android.Content.Res.AssetManager Manager = this.Assets;
            using (var Reader =
                new System.IO.StreamReader(Manager.Open("Contenido.txt")))
            {
                CounterHeader.Text += $"\n\n{Reader.ReadToEnd()}";
            }
        }
    }
}

