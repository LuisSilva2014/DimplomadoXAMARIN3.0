﻿using Android.App;
using Android.Widget;
using Android.OS;
using HackAtHome.SAL;
using HackAtHome.Entities;
using System.Threading.Tasks;
using Android.Content;

namespace Hack_Home
{
    [Activity(Label = "@string/ApplicationName", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {

      public static readonly System.Collections.Generic.List<string> LAs =
       new System.Collections.Generic.List<string>();

        protected override void OnCreate(Bundle bundle)
        {

            //Puede ir arraba
            base.OnCreate(bundle);


            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

          
            FindViewById<Button>(Resource.Id.buttonButtonValidate).Click += (sS, e) =>
            {
                ServiceClient Obj = new ServiceClient();
                Task<ResultInfo> R = Obj.AutenticateAsync("l.r.s.m.2009@hotmail.com", "luissilva2013");

                if (R.Result.ToString() == "SDS")
                {
                    var intent = new Intent(this, typeof(ListActivities));
                    intent.PutStringArrayListExtra("ListActivities", LAs);
                    //Pasamos el token entre actividades
                    intent.PutExtra("ResultTokenCredentialsValue", R.Result.Token.ToString());
                    StartActivity(intent);
                }
            };


        }
    }
}

